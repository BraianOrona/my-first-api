package com.folcademy.myfirstapp.controllers;

import org.springframework.web.bind.annotation.*;

@RestController
public class PruebaController {
    @GetMapping("/")
    public String show(){
        return "Se mostro correctamente. ";
    }
    @DeleteMapping("/")
    public String delete(){
        return "Se elimino correctamente. ";
    }
    @PostMapping("/")
    public String create(){
        return "Se creo correctamente.";
    }
    @PutMapping("/")
    public String modify(){
        return "Se modifico correctamente.";
    }
}

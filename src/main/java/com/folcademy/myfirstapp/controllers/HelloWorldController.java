package com.folcademy.myfirstapp.controllers;

import org.springframework.web.bind.annotation.*;

@RestController
public class HelloWorldController {
    @GetMapping("/Hello")
    public String helloWorld(@RequestParam(value = "name", defaultValue = "World")String name){
        return "Hello "+name+"!!";
    }

}
